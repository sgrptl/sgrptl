---
title: "Migrating Away From Bower"
date: 2020-10-16T10:40:25+01:00
draft: false
tags: ["til", "bower", "yarn", "migrating from bower", "nodejs"]
---
At work, one of the systems that is seldom updated was still using Bower to fetch its client side requirements. Bower has an [official migration guide][1] that is super easy to use and follow. It does however, add a dependency on [Yarn][2] which could be an issue if you don't already use it but it wasn't in our case.

At a high level, what `bower-away` (the tool used for the migration) does is it scans through the `bower_components` directory and converts these Bower dependencies in `bower.json` to `@bower_components/<package-name>` packages in `package.json` with the actual Bower package's Git repo linked. So, `package.json` would now look something like this:

{{<highlight json "linenos=true">}}
{
  ...
  "dependencies": {
    "@bower_components/angular" : "angular/bower-angular#^1.0.8",
    "@bower_components/d3" : "mbostock-bower/d3-bower#~3.3.10"
    ....
  }
  ...
}
{{</highlight>}}

Yarn is able to resolve the semver ranges on the git tags and install these packages. `bower-away` also adds a `postinstall` script to `package.json` which points any references to `bower_components` to `node_modules/@bower_components`.

[1]: https://bower.io/blog/2017/how-to-migrate-away-from-bower/
[2]: https://yarnpkg.com
