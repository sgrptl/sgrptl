---
title: "Rabbitmq PRECONDITION_FAILED - inequivalent arg"
date: 2020-11-05T05:45:52Z
draft: false
tags: ["ruby", "rabbitmq", "hutch", "precondition_failed"]
---
I recently upgraded the [hutch][1] gem to `v0.28.0` for one of our applications. As I deployed it to production, the workers refused to start and failed with the following exception:
{{<highlight ruby "linenos=false">}}
406 PRECONDITION_FAILED - inequivalent arg 'x-queue-type' for queue <queuename> in vhost '/': received 'default' of type 'longstr' but current is none
{{</highlight>}}

I'm not a RabbitMQ expert so I [looked the error up][2]. The error basically means that the Consumer and the Queue are not on the same page i.e. their exchange parameters don't match. In my case, the Consumer hadn't set the `x-queue-type` parameter while the Queue had it set to `default`. Upgrading to `v1.0.0` fixed this as it reverts some parameters that the client library was setting. From the [changelog][3],
> By default Hutch will not configure any `x-queue-type` or `x-queue-mode` optional arguments which is identical to RabbitMQ defaults (a regular classic queue).


[1]: https://github.com/ruby-amqp/hutch
[2]: https://groups.google.com/forum/#!topic/rabbitmq-users/rwhtLMrZ1rY
[3]: https://github.com/ruby-amqp/hutch/blob/master/CHANGELOG.md#100-april-8th-2020