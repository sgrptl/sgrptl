---
title: "Grape Roar Presenter Issues"
date: 2020-10-21T06:40:50+01:00
draft: false
tags: ["ruby", "grape-roar", "representable", "roar", "presenter"]
---
[`grape-roar`][1] is an amazing Ruby gem that makes it easy to parse and render REST API documents using Presenters in [`Grape`][2]. While upgrading from a pretty old version, it broke one of the tests. Took me quite a bit to figure out what actually was going wrong so putting this out for easier recall.

The breakdown was something like this. We had a presenter class:
{{<highlight ruby "linenos=true">}}
class SongPresenter < Grape::Roar::Decorator
    include Representable::Hash
    include Representable::Hash::AllowSymbols

    property :title
    property :artist
end
{{</highlight>}}

We were calling it in this manner:
{{<highlight ruby "linenos=true">}}
desc 'create a song' do
...
end
post :create_song do
    song_params = params.merge record_created_by: logged_in_user
    song_representation = SongPresenter.new(song_params)
end
{{</highlight>}}

After the upgrade to the latest `grape-roar`, `representable` and `roar` versions, this broke the POST API with the following error:
{{<highlight ruby "linenos=false">}}
NoMethodError: undefined method `title` for <ActiveSupport::HashWithIndifferentAccess:0x...>
{{</highlight>}}

which looks like a very weird error. After racking my brains for quite some time, it occurred to me that in the documentation for all these gems they've always specified a class as the input to the `SongPresenter.new` constructor. 

I changed the line
{{<highlight ruby "linenos=table,hl_lines=5">}}
desc 'create a song' do
...
end
post :create_song do
    song_params = params.merge record_created_by: logged_in_user
    song_representation = SongPresenter.new(song_params)
end
{{</highlight>}}
to
{{<highlight ruby "linenos=table,hl_lines=5">}}
desc 'create a song' do
...
end
post :create_song do
    song_params = OpenStruct.new(params.merge record_created_by: logged_in_user)
    song_representation = SongPresenter.new(song_params)
end
{{</highlight>}}
and it started working again. There is an open issue on the [grape-roar][3] repo since July 2015 that talks about the exact same problem!!! Might be a good candidate for some PRs... 😎🤓

At the same time, it looks like there hasn't been any activity on the [`grape-roar`][1] repo in a while, so might be a good idea to migrate away from that dependency to avoid the same pains in the future.

[1]: https://github.com/ruby-grape/grape-roar
[2]: https://github.com/ruby-grape/grape
[3]: https://github.com/ruby-grape/grape-roar/issues/14