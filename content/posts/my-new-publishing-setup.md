---
title: "My new setup for publishing posts"
date: 2023-01-02T00:00:00+00:00
draft: false
---

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [The Previous Setup](#the-previous-setup)
- [Lucky strike](#lucky-strike)
- [Setup](#setup)
- [Conclusion](#conclusion)

</div>
<!--endtoc-->

I use [Hugo](https://gohugo.io/) for this website. Recently, I rediscovered [OrgMode](https://orgmode.org/) and I decided to give it a go as my note taking mechanism. Consequently, it only made sense to also translate posts on this website into this new setup. This post highlights the setup and the new process.


## The Previous Setup {#the-previous-setup}

But first, let's look at the previous setup! The previous setup was the plain vanilla Hugo setup. All my posts were inside a `content/` folder. They were written in Markdown. Things were fine really!

I didn't really have a problem with the setup. The only friction was having to switch between OrgMode and Markdown depending on the context. Anyone trying to learn a new language understands that to achieve native fluency in the new language  requires you to start _"thinking"_ in the new language, embrace its idioms -- idiosyncracies and all!


## Lucky strike {#lucky-strike}

Fortunately for me, [OrgMode already supports exporting to Markdown](https://orgmode.org/manual/Markdown-Export.html)!! This made the transition so much easier... In my head, the New Way Of  Doing Things looked like this:

1.  Start a post in `content-org/` folder. Write it in  OrgMode.
2.  Export the post to Markdown. The exported file would live in the `content/` folder.
3.  Win!


## Setup {#setup}

The first attempt at a plain Markdown worked! A minor inconvenience was that I needed to manually specify the output file. Again, fortunately for me, some awesome folks have already improved on this pain point and made it seamless (:chef's kiss:)!! Yet another moment I'm grateful that Open Source Software exists and is thriving!

For me (using Doom Emacs), this was as simple as adding the following line to my `init.el` file.

{{< highlight elisp >}}
;; under the :lang section find the line saying org and change it to:
(org +hugo)
{{< /highlight >}}

With this in place, all I needed to add to the top of my `post.org` file was:

{{< highlight orgmode >}}
#+HUGO_BASE_DIR: ../
#+HUGO_SECTION: posts
{{< /highlight >}}

This would automatically export my Markdown posts to the `content/posts/` folder. Additional configuration for `ox-hugo` (the OrgMode plugin that makes this possible) can be found on [the plugin's website](https://ox-hugo.scripter.co/).

One final addition to make the whole experience seamless was to enable auto-export on save. To do this, I needed to create a `.dir-locals.el` file at the root of my Hugo website and add the following content to it:

{{< highlight elisp >}}
(("content-org/"
  . ((org-mode . ((eval . (org-hugo-auto-export-mode))
                  (setq 'ox-hugo-export-with-section-numbers t))))))
{{< /highlight >}}


## Conclusion {#conclusion}

While it may seem a lot of effort and yak-shaving to reduce a minor friction point, I want to use OrgMode as my lingua franca so I believe it is worth the effort.
