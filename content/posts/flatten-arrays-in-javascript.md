---
title: "Flatten Arrays in Javascript"
date: 2020-10-20T06:42:27+01:00
draft: false
tags: ["javascript", "flatten array", "nodejs"]
---
Yesterday, I encountered [`Array.prototype.flat`][1] for the first time. I didn't know it existed😱😱😱!! Very cool function...

Its job is to flatten nested arrays into a single array.
{{<highlight js "linenos=false">}}
var a = [[1, 2], [[3, 4], [5, 6]], 7, 8];
var b = a.flat()
//[1, 2, 3, 4, 5, 6, 7, 8]
{{</highlight>}}

It can also flatten upto a given level.
{{<highlight js "linenos=false">}}
var a = [[1, 2], [[3, 4], [5, 6]], 7, 8];
var b = a.flat(1)
//[1, 2, [3, 4], [5, 6], 7, 8]
{{</highlight>}}

This was extremely useful when I was trying to flatten an array for the [ElasticSearch Bulk API][2]. I had a list of objects that looked like this:
{{<highlight js "linenos=false">}}
var bulkOps = [
        [{ "delete" : { "_index" : "test", "_id" : "2" } }],
        [{ "create" : { "_index" : "test", "_id" : "3" } }, { "field1" : "value3" }],
        [{ "update" : {"_id" : "1", "_index" : "test"} }, { "doc" : {"field2" : "value2"} }]
    ];
{{</highlight>}}

`bulkOps.flat(1)` was all I needed to transform it into the shape that the Bulk API needed. 

> Note that in this case `bulkOps.flat()` would have issues flattening the inner objects since `bulkOps` is not a nested list all the way down.

[1]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/flat
[2]: https://www.elastic.co/guide/en/elasticsearch/reference/7.x/docs-bulk.html