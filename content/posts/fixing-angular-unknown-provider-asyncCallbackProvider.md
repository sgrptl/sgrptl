---
title: "Fixing Angular Unknown Provider $$asyncCallbackProvider"
date: 2020-10-19T02:54:02+01:00
draft: false
---
While trying to upgrade from Angular 1.5 to 1.8 on a legacy system, I encountered a strange error in our tests.

{{<highlight js "linenos=false">}}
Error: [$injector:modulerr] Failed to instantiate module ngMock due to:
Error: [$injector:unpr] Unknown provider: $$asyncCallbackProvider
{{</highlight>}}

The fix is fairly straightforward: we just need to keep the `angular` version and the `angular-mocks` version in sync i.e. **both the packages should use the same version**. After that, a quick `yarn install` and voila!