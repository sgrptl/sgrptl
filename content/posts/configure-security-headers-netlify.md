---
title: "Configure Security Headers Netlify"
date: 2020-10-15T21:04:24+01:00
draft: false
tags: ["til", "netlify", "hugo", "webpagetest"]
---

On [WebPageTest](https://www.webpagetest.org/), this site was scoring very poorly on the Security Score. So I decided to fix that. Since this site is deployed on Netlify, based on their [documentation][1], all I needed to do was add the required headers to my `netlify.toml` file (at the end).

{{<highlight toml "linenos=true">}}
[[headers]]
  for = "/*"
  [headers.values]
    Content-Security-Policy = "default-src 'self';font-src 'self' fonts.gstatic.com;style-src 'self' fonts.googleapis.com"
    X-Content-Type-Options = "nosniff"
    X-Frame-Options = "SAMEORIGIN"
    X-XSS-Protection = "1; mode=block"
{{</highlight>}}

Your value for the `Content-Security-Policy` header may vary based on the resources you use. MDN has some great examples on its [CSP page][2].

And voila! The rating shot to an A+!!!

[1]: https://docs.netlify.com/routing/headers/#syntax-for-the-netlify-configuration-file
[2]: https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP#Examples_Common_use_cases
