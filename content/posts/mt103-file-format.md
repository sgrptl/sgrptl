---
title: "MT103 File Format"
date: 2020-10-14T06:59:53+01:00
draft: false
tags: ["til", "money transfer", "SWIFT"]
---

[MT103](https://en.wikipedia.org/wiki/MT103) is a SWIFT message format used for cross border money transfer.