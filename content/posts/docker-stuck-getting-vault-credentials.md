---
title: "Docker Stuck Getting Vault Credentials"
date: 2020-11-18T08:41:02Z
draft: false
tags: ["envconsul hangs", "envconsul", "docker", "consul", "vault"]
---

In one of the CI workflows we have at work, I was trying to run a Docker image with a custom `run` command. However, since this was on a CI machine, it wasn't able to find a valid Consul/Vault server and would simply keep retrying connecting to the server. As a result, the application just wouldn't start...

The logs looked something like this:
{{<highlight docker "linenos=false">}}
RL: GET https://vault:8200/v1/_vault/non-existing
Code: 403. Errors:

* permission denied (retry attempt 1 after "250ms")
2018/03/22 09:52:17.387832 [ERR] (view) vault.read(_vault/non-existing): vault.read(_vault/non-existing): Error making API request.

URL: GET https://vault:8200/v1/_vault/non-existing
Code: 403. Errors:

* permission denied (exceeded maximum retries)
2018/03/22 09:52:17.387918 [ERR] (runner) watcher reported error: vault.read(_vault/non-existing): vault.read(_vault/non-existing): Error making API request.
{{</highlight>}}

There's an open issue on the [envconsul repo][2] about the same. 

It turns out that an easy way to fix this is to provide a [Docker Entrypoint][1] argument to the run command. In my case, passing `--entrypoint []` did the trick.

[1]: https://docs.docker.com/engine/reference/builder/#entrypoint
[2]: https://github.com/hashicorp/envconsul/issues/169