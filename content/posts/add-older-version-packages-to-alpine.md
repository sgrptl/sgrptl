---
title: "Add Older Version Packages to Alpine"
date: 2020-10-13T05:01:43+01:00
draft: false
categories: ["TIL"]
tags: ["til", "alpine", "ruby", "image upgrade"]
---
At work, we use `Ruby 2.5.8` and the base image was `alpine:3.10`. We wanted to upgrade the base image to `alpine:3.12`. 

## First attempt
{{<highlight dockerfile "linenos=true">}}
FROM alpine:3.12 as base
RUN apk add --no-cache ruby
{{</highlight>}}
ends up installing `Ruby 2.7.1-r3`. 

## Second Attempt 
Then I try to pin the package version like
{{<highlight dockerfile "linenos=true">}}
FROM alpine:3.12 as base
RUN apk add --no-cache ruby=2.5.8
{{</highlight>}}
but that results in this error output

{{<highlight dockerfile "linenos=true">}}
ERROR: unsatisfiable constraints:
   ruby-2.7.1-r3:
     breaks: world[ruby=2.5.8]
{{</highlight>}}

## Solution
To fix this, we need to add the add the repository containing the package version in `/etc/apk/repositories` and then install the pinned version. Also add the `--update` option to `apk add`.
{{<highlight dockerfile "linenos=true">}}
FROM alpine:3.12 as base
RUN echo "http://dl-cdn.alpinelinux.org/alpine/v3.10/main" >> /etc/apk/repositories && apk add --no-cache --update ruby=2.5.8
{{</highlight>}}