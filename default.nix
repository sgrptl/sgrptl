let
  sources = import ./nix/sources.nix;
  pkgs = import sources.nixpkgs { overlays = [ ] ; config = {}; };
  
  nodePackages = with pkgs.nodePackages; [
    node2nix
  ];
in pkgs.mkShell {
  buildInputs = [
    pkgs.nodejs-18_x
  ] ++ nodePackages;
}
