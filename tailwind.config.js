/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./layouts/**/*.html"],
  theme: {
    extend: {
      colors: {
        'midnight': 'rgb(17 24 39)'
      }
    },
  },
  plugins: [],
  darkMode: 'media'
}
